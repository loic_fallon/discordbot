import os
import random
import dico
import utils

import discord
from dotenv import load_dotenv


load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
GUILD = os.getenv('DISCORD_GUILD')

intents = discord.Intents.all()
client = discord.Client(intents=intents)

_ptitpote = []
_dictator = ""


@client.event
async def on_ready():
    print(f'{client.user.name} has connected to Discord!')
    for role in client.guilds[0].roles:
        if role.name == "Ptit pote":
            for user in role.members:
                _ptitpote.append(user)


@client.event
async def on_message(message):

    global _dictator
    if message.author == client.user:
        return

    text = message.content.split("%")
    if text[0] == '':

        commande = text[1].split(" ", 1)
        word = commande[0].upper()

        if word == 'DICTATEUR':
            if _dictator == "":
                _dictator = random.choice(_ptitpote)
                while _dictator.status != discord.Status.online:
                    print(_dictator)
                    _dictator = random.choice(_ptitpote)

                _dictateurRole = discord.utils.get(client.guilds[0].roles, name="Dictateur démocratique")
                for member in _dictateurRole.members:
                    await member.remove_roles(_dictateurRole)

                await _dictator.add_roles(_dictateurRole)
                await message.channel.send(utils.setText("Le dictateur est ... " + _dictator.name + " !!!"))

            else:
                await message.channel.send(utils.setText("TROP TARD " + _dictator.name.upper() + " EST DEJA DANS LA PLACE !!!"))

        elif word == 'GROS':
            await message.channel.send(utils.setText(message.author.name + " ==> " + str(random.randint(40, 150)) + "kg"))

        elif word == 'HURLE':
            await message.channel.send(utils.setText(utils.JEHURLE(commande[1])))

    else:
        text = message.content.split(" ")
        for word in text:
            word = word.lower()

            if word in dico.miam:
                await message.channel.send(utils.setText(utils.JEHURLE("manger")))

            elif word in dico.lolo:
                await message.channel.send(utils.setText(random.choice(dico.lolobg)))

            elif word == "alexandre":
                await message.channel.send(utils.setText("ALEKZANDREEEE"))

            elif word == "saucisse":
                await message.channel.send(utils.setText("ZAUZISSSSSS"))

client.run(TOKEN)
